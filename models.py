from application import app
from db import db

class PCDB_PRODUCT(db.Model):
    biib_code = db.Column(db.String(50), primary_key=True)
    partner = db.Column(db.String(50))
    modality = db.Column(db.String(50))
    
    def __init__(self, biib_code, partner, modality):
        self.biib_code = biib_code
        self.partner = partner
        self.modality = modality
    
    def __repr__(self):
        return "%s" % (self.biib_code)  
    
class PCDB_PRODUCT_ALIAS(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    biib_code = db.Column(db.String(50))
    alias = db.Column(db.String(50))
    alias_type = db.Column(db.String(50))

    def __init__(self, biib_code, alias, alias_type):
        self.biib_code = biib_code
        self.alias = alias
        self.alias_type = alias_type

    def __repr__(self):
        return "%s (%s)" % (self.biib_code, self.alias)
    
class PCDB_PRODUCT_PROCESS(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    biib_code = db.Column(db.String(50))
    mfg_mode = db.Column(db.String(50))
    process_identifier = db.Column(db.String(50))
    process_description = db.Column(db.String(50))
    process_status = db.Column(db.String(50))
    
    def __init__(self, biib_code, mfg_mode, process_identifier, process_description, process_status):
        self.biib_code = biib_code
        self.mfg_mode = mfg_mode
        self.process_identifier = process_identifier
        self.process_description = process_description 
        self.process_status = process_status
        
    def __repr__(self):
        return "(%s) %s - %s - %s" % (self.biib_code, self.mfg_mode, self.process_identifier, self.process_status)

class PCDB_OPERATION(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    process_id = db.Column(db.Integer)
    biib_code = db.Column(db.String(50))
    mfg_mode = db.Column(db.String(50))
    operation_order = db.Column(db.String(50))
    operation_name = db.Column(db.String(50))
    operation_type = db.Column(db.String(50))
    chrom_type = db.Column(db.String(50))
    adjustment_type = db.Column(db.String(50))
    adjustment_alias = db.Column(db.String(50))
    cycle_pool = db.Column(db.String(50))
    bpr_order = db.Column(db.String(50))
    operation_full_name = db.Column(db.String(100))
    
    def __init__(self, process_id, biib_code, mfg_mode, operation_order, operation_name, operation_type, chrom_type, adjustment_type, adjustment_alias, cycle_pool, bpr_order, operation_full_name):
        self.process_id = process_id
        self.biib_code = biib_code
        self.mfg_mode = mfg_mode
        self.operation_order = operation_order
        self.operation_name = operation_name
        self.operation_type = operation_type 
        self.chrom_type = chrom_type
        self.adjustment_type = adjustment_type
        self.adjustment_alias = adjustment_alias
        self.cycle_pool = cycle_pool
        self.bpr_order = bpr_order
        self.operation_full_name = operation_full_name
        
    def __repr__(self):
        return "%s %s %s" % (self.operation_order, self.operation_type, self.operation_name)
    
class PCDB_SAMPLE(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    operation_type = db.Column(db.String(50))
    sample_order = db.Column(db.Integer)
    sample_name = db.Column(db.String(50))
    
    def __init__(self, operation_type, sample_order, sample_name):
        self.operation_type = operation_type
        self.sample_order = sample_order
        self.sample_name = sample_name
        
    def __repr__(self):
        return "%s - %s" % (self.operation_type, self.sample_order, self.sample_name)
        
class PCDB_MEASURE(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    measure_name = db.Column(db.String(250))
    units_of_measure = db.Column(db.String(50))
    data_type = db.Column(db.String(50))
    
    def __init__(self, measure_name, units_of_measure, data_type):
        self.measure_name = measure_name
        self.units_of_measure = units_of_measure
        self.data_type = data_type
        
    def __repr__(self):
        return "%s (%s)" % (self.measure_name, self.units_of_measure)
    
class PCDB_PARAMETERS(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    operation_id = db.Column(db.Integer)
    sample_id = db.Column(db.Integer)
    measure_id = db.Column(db.Integer)
    parameter_name = db.Column(db.String(200))
    sample_type = db.Column(db.String(50))
    decimal_display = db.Column(db.String(50))
    target = db.Column(db.String(50))
    lor = db.Column(db.String(50))
    uor = db.Column(db.String(50))
    lal = db.Column(db.String(50))
    ual = db.Column(db.String(50))
    lsl = db.Column(db.String(50))
    usl = db.Column(db.String(50))
    lcl = db.Column(db.String(50))
    ucl = db.Column(db.String(50))
    classification = db.Column(db.String(50))
    mapping_replicates = db.Column(db.Integer)
    
    def __init__(self, operation_id, sample_id, measure_id):
        self.operation_id = operation_id
        self.sample_id = sample_id
        self.measure_id = measure_id
        
    def __repr__(self):
        return "%s" % (self.sample_type)

class PCDB_PRIMR(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    parameter_id = db.Column(db.Integer)
    parameter_iteration = db.Column(db.Integer)
    primr_book_template_name = db.Column(db.String(255))
    primr_page_name = db.Column(db.String(255))
    primr_field_name= db.Column(db.String(255))
    primr_row_number= db.Column(db.String(255))
    
    def __init__(self, parameter_id, parameter_iteration, primr_book_template_name, primr_page_name, primr_field_name, primr_row_number):
        self.parameter_id = parameter_id
        self.parameter_iteration = parameter_iteration
        self.primr_book_template_name = primr_book_template_name
        self.primr_page_name = primr_page_name
        self.primr_field_name = primr_field_name
        self.primr_row_number = primr_row_number
        
    def __repr__(self):
        return "%s (%s)" % (self.primr_field_name)

class PCDB_PARAMETERS_V(db.Model):    
    id = db.Column(db.Integer, primary_key=True)
    process_id = db.Column(db.Integer)
    operation_id = db.Column(db.Integer)
    operation_name = db.Column(db.String(100))
    operation_type = db.Column(db.String(100))
    operation_order = db.Column(db.String(50))
    adjustment_type = db.Column(db.String(100))
    sample_id = db.Column(db.Integer)
    sample_name = db.Column(db.String(50))
    sample_order = db.Column(db.String(50))
    measure_id = db.Column(db.Integer)
    measure_name = db.Column(db.String(250))
    units_of_measure = db.Column(db.String(100))
    sample_type = db.Column(db.String(50))
    parameter_name = db.Column(db.String(200))
    decimal_display = db.Column(db.String(50))
    target = db.Column(db.String(50))
    lor = db.Column(db.String(50))
    uor = db.Column(db.String(50))
    lal = db.Column(db.String(50))
    ual = db.Column(db.String(50))
    lsl = db.Column(db.String(50))
    usl = db.Column(db.String(50))
    lcl = db.Column(db.String(50))
    ucl = db.Column(db.String(50))
    classification = db.Column(db.String(50))
    mapping_replicates = db.Column(db.String(50))
        
    def __repr__(self):
        return "%s" % (self.sample_type)
    
class PCDB_PRIMR_V(db.Model):
    mapping_id = db.Column(db.Integer, primary_key=True)
    parameter_id = db.Column(db.Integer)
    parameter_iteration = db.Column(db.Integer)
    primr_book_template_name = db.Column(db.String(255))
    primr_page_name = db.Column(db.String(255))
    primr_field_name= db.Column(db.String(255))
    primr_row_number= db.Column(db.String(255))
    process_id = db.Column(db.Integer)
    operation_name = db.Column(db.String(100))
    operation_type = db.Column(db.String(100))
    adjustment_type = db.Column(db.String(100))
    sample_name = db.Column(db.String(50))
    measure_name = db.Column(db.String(250))
    units_of_measure = db.Column(db.String(100))
    mapping_replicates = db.Column(db.String(50))
    
    def __repr__(self):
        return "%s (%s)" % (self.primr_field_name)
    
class PCDB_DELTAV(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    parameter_id = db.Column(db.Integer)
    dv_unit = db.Column(db.String(255))
    dv_process_phase = db.Column(db.String(255))
    dv_category = db.Column(db.String(255))
    dv_cycle = db.Column(db.String(255))
    dv_xlt_process_name = db.Column(db.String(255))
    dv_unit_procedure = db.Column(db.String(255))
    dv_iteration = db.Column(db.String(255))
    dv_output_col = db.Column(db.String(255))
    
    def __init__(self,parameter_id,dv_unit,dv_process_phase,dv_category,dv_cycle,dv_xlt_process_name,dv_unit_procedure,dv_iteration,dv_output_col):
        self.parameter_id = parameter_id
        self.dv_unit = dv_unit
        self.dv_process_phase = dv_process_phase
        self.dv_category = dv_category
        self.dv_cycle = dv_cycle
        self.dv_xlt_process_name = dv_xlt_process_name
        self.dv_unit_procedure = dv_unit_procedure
        self.dv_iteration = dv_iteration
        self.dv_output_col = dv_output_col
        
    def __repr__(self):
        return "%s (%s)" % (self.parameter_id)
   
class PCDB_DELTAV_V(db.Model):
    mapping_id = db.Column(db.Integer, primary_key=True)
    parameter_id = db.Column(db.Integer)
    process_id = db.Column(db.Integer)
    operation_name = db.Column(db.String(100))
    operation_type = db.Column(db.String(100))
    adjustment_type = db.Column(db.String(100))
    sample_name = db.Column(db.String(50))
    measure_name = db.Column(db.String(250))
    units_of_measure = db.Column(db.String(100))
    dv_unit = db.Column(db.String(255))
    dv_process_phase = db.Column(db.String(255))
    dv_category = db.Column(db.String(255))
    dv_cycle = db.Column(db.String(255))
    dv_xlt_process_name = db.Column(db.String(255))
    dv_unit_procedure = db.Column(db.String(255))
    dv_iteration = db.Column(db.String(255))
    dv_output_col = db.Column(db.String(255))
    mapping_replicates = db.Column(db.String(50))
    
    def __repr__(self):
        return "%s (%s)" % (self.mapping_id)
    
class PCDB_AUDIT_TRAIL(db.Model):
    ID = db.Column(db.Integer, primary_key=True)
    date_time = db.Column(db.DateTime)
    change_type = db.Column(db.String(50))
    table_name = db.Column(db.String(100))
    row_id = db.Column(db.String(100))
    column_name = db.Column(db.String(100))
    original_value = db.Column(db.String(100))
    updated_value = db.Column(db.String(100))
    
    def __init__(self, date_time, change_type, table_name, row_id, column_name, original_value, updated_value):
        self.date_time = date_time
        self.table_name = table_name
        self.change_type = change_type
        self.row_id = row_id
        self.column_name = column_name
        self.original_value = original_value
        self.updated_value = updated_value
        
class PCDB_LES(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    parameter_id = db.Column(db.Integer)
    parameter_iteration = db.Column(db.Integer)
    sample_product = db.Column(db.String(100))
    sample_stage = db.Column(db.String(100))
    test_analysis = db.Column(db.String(100))
    result_name = db.Column(db.String(100))
    
    def __init__(self,parameter_id,parameter_iteration,sample_product,sample_stage,test_analysis,result_name):
        self.parameter_id = parameter_id
        self.parameter_iteration = parameter_iteration
        self.sample_product = sample_product
        self.sample_stage = sample_stage
        self.test_analysis = test_analysis
        self.result_name = result_name

    def __repr__(self):
        return "%s (%s)" % (self.parameter_id)
    
class PCDB_LES_V(db.Model):
    parameter_id = db.Column(db.Integer)
    process_id = db.Column(db.Integer)
    mapping_id = db.Column(db.Integer, primary_key=True)
    operation_name = db.Column(db.String(100))
    operation_type = db.Column(db.String(100))
    adjustment_type = db.Column(db.String(100))
    sample_name = db.Column(db.String(100))
    measure_name = db.Column(db.String(100))
    units_of_measure = db.Column(db.String(100))
    parameter_iteration = db.Column(db.Integer)
    sample_product= db.Column(db.String(100))
    sample_stage = db.Column(db.String(100))
    test_analysis = db.Column(db.String(100))
    result_name = db.Column(db.String(100))
    mapping_replicates = db.Column(db.String(50))
    
class PCDB_DVPI(db.Model):
    id= db.Column(db.Integer, primary_key=True)
    parameter_id = db.Column(db.Integer)
    parameter_iteration = db.Column(db.Integer)
    unitprocedure = db.Column(db.String(250))
    operation = db.Column(db.String(250))
    phase = db.Column(db.String(250))
    attribute = db.Column(db.String(250))
    
    def __init__(self,parameter_id,parameter_iteration,unitprocedure,operation,phase,attribute):
        self.parameter_id = parameter_id
        self.parameter_iteration = parameter_iteration
        self.unitprocedure = unitprocedure
        self.operation = operation
        self.phase = phase
        self.attribute =attribute
        
class PCDB_DVPI_V(db.Model):
    mapping_id = db.Column(db.Integer, primary_key=True)
    parameter_id = db.Column(db.Integer)
    parameter_iteration = db.Column(db.Integer)
    process_id = db.Column(db.Integer)
    operation_name = db.Column(db.String(100))
    operation_type = db.Column(db.String(100))
    adjustment_type = db.Column(db.String(100))
    sample_name = db.Column(db.String(50))
    measure_name = db.Column(db.String(250))
    units_of_measure = db.Column(db.String(100))
    mapping_replicates = db.Column(db.String(50))
    unitprocedure = db.Column(db.String(250))
    operation = db.Column(db.String(250))
    phase = db.Column(db.String(250))
    attribute = db.Column(db.String(250))